json.array! @posts do |post|
  json.extract! post, :id, :title, :body, :parent_id, :root_id, :deleted, :created_at, :updated_at
end
