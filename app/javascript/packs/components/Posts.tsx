import React, {useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import Modal from 'react-modal';
import queryString from 'query-string';

import 'components/Posts.scss';

const PostsComponent = ({ location }) => {
    const history = useHistory();
    // const [posts, originalSetPosts] = React.useState(localStorage.getItem('posts') ? JSON.parse(localStorage.getItem('posts')) as any : []);
    const [posts, originalSetPosts] = React.useState([]);
    const [searchQuery, setSearchQuery] = React.useState<string>('');

    const setPosts = (newPosts) => {
        if (JSON.stringify(posts) !== JSON.stringify(newPosts)) {
            // localStorage.setItem('posts', JSON.stringify(newPosts));
            originalSetPosts(newPosts);
        }
    };

    const fetchPosts = () => {
        fetch('/api/posts')
            .then((response) => response.json())
            .then((response) => {
                setPosts(response);
            })
            .catch((response) => {
                console.error(response);
            })
    };

    const search = (text) => {
        fetch(`/api/posts/search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                text
            })
        })
            .then((response) => response.json())
            .then((response) => {
                setPosts(response);
            })
            .catch((response) => {
                console.error(response);
            })
    };

    const qs = queryString.parse(location.search);
    useEffect(() => {
        if (!qs || !qs['q']) {
            fetchPosts();
        } else {
            setSearchQuery(qs['q'] as string);
            search(qs['q'] as string);
        }
    }, [location]);

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [targetPost, setTargetPost] = React.useState(null);
    const closeModal = () => setIsOpen(false);

    const deletePost = (id: number) => {
        fetch(`/api/posts/${id}`, {
            method: 'DELETE'
        })
            .then(() => {
                fetchPosts();
            })
    };

    const handleDeletePost = (id: number) => {
        deletePost(id);
        setIsOpen(false);
    };

    const postsDom = posts.map((post) => {
        return (
            <div key={post.id}>
                <Link to={`/p/${post.root_id}`}>{post.title}</Link>
                <button onClick={() => {
                    setTargetPost(post);
                    setIsOpen(true);
                }}>x</button>
            </div>
        );
    });

    const handleSearchBox = (e) => {
        if (e.key == 'Enter') {
            e.preventDefault();
            history.push(`/?q=${searchQuery}`);
        }
    };

    return (
        <div id="content">
            <div id="header">
                <nav id="navbar">
                    <div>
                        <button onClick={() => {
                            history.push('/');
                            search('');
                        }}>niki</button>
                    </div>
                    <div>
                        <input value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} onKeyPress={handleSearchBox} />
                        <button onClick={() => history.push('/p')}>CREATE</button>
                    </div>
                </nav>
            </div>
            <div id="main">
                <div id="posts">
                    {postsDom}
                </div>
            </div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                contentLabel="delete post"
            >
                <p>{targetPost && targetPost.title} を消す？</p>
                <button onClick={() => handleDeletePost(targetPost.id)}>Yes</button>
            </Modal>
        </div>
    );
};

export default PostsComponent;
