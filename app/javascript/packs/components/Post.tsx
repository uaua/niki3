import React, { useEffect, useRef } from 'react';
import * as HyperMD from "hypermd";
import { toast } from 'react-toastify';
import Modal from 'react-modal';
import * as Diff from 'diff';
import * as Diff2html  from 'diff2html';
import { useHistory } from "react-router-dom";

import 'components/Post.scss';
import 'diff2html/bundles/css/diff2html.min.css';
import {OutputFormatType} from "diff2html/lib/types";

const initializeToc = ($toc, cm) => {
    let lastTOC = "";

    const update = HyperMD.debounce(function () {
        let newTOC = "";
        cm.eachLine(function (line) {
            let tmp = /^(#+)\s+(.+)(?:\s+\1)?$/.exec(line.text);
            if (!tmp) return;
            let lineNo = line.lineNo();
            if (!cm.getStateAfter(lineNo).header) return; // double check but is not header
            let level = tmp[1].length;

            let title = tmp[2];
            title = title.replace(/([*_]{1,2}|~~|`+)(.+?)\1/g, '$2'); // em / bold / del / code
            title = title.replace(/\\(?=.)|\[\^.+?]|!\[((?:[^\\\]]+|\\.)+)](\(.+?\)| ?\[.+?])?/g, ''); // images / escaping slashes / footref
            title = title.replace(/\[((?:[^\\\]]+|\\.)+)](\(.+?\)| ?\[.+?])/g, '$1'); // links
            title = title.replace(/&/g, '&amp;');
            title = title.replace(/</g, '&lt;');
            newTOC += '<div data-line="' + lineNo + '" class="toc-item" style="padding-left:' + level + 'em">' + title + '</div>';
        });
        if (newTOC === lastTOC) return;
        $toc.innerHTML = lastTOC = newTOC;
    }, 300);

    cm.on('changes', update);

    $toc.addEventListener('click', function (ev) {
        let t = ev.target;
        if (!/toc-item/.test(t.className)) return;
        let lineNo = ~~t.getAttribute('data-line');
        cm.setCursor({ line: cm.lastLine(), ch: 0 });
        setTimeout(function () {
            cm.setCursor({ line: lineNo, ch: 0 });
        }, 10);
    }, true);
};

const displayError = (errors) => {
    for (const e of errors) {
        toast.error(errors[e].join());
    }
};

const PostComponent = ({ match: { params } }) => {
    const textAreaElement = useRef(null);
    const tocElement = useRef(null);
    const editorElement = useRef(null);
    const history = useHistory();

    const [post, setPost] = React.useState(null);
    const [editor, setEditor] = React.useState(null);
    const [title, setTitle] = React.useState('');
    const [editable, setEditable] = React.useState(true);
    const [diffs, setDiffs] = React.useState([]);

    const fetchPost = (id) => {
        fetch(`/api/posts/${id}`)
            .then((response) => response.json())
            .then((response) => {
                if (!('error' in response)) {
                    setPost(response);
                } else {
                    displayError(response);
                }
            })
            .catch((response) => {
                console.error(response);
            })
    };

    useEffect(() => {
        if (params.id) {
            fetchPost(params.id);
        } else {
            setPost(null);
        }
    }, [params.id]);

    useEffect(() => {
        const editor = HyperMD.fromTextArea(textAreaElement.current, {
            hmdHideToken: false
        });
        setEditor(editor);
    }, [textAreaElement]);

    useEffect(() => {
        if (editor) {
            initializeToc(tocElement.current, editor);

            if (post) {
                if (editor.getValue() !== post.body) {
                    editor.setValue(post.body); // do not call setValue before initializeToc to fire 'changes' event
                }
                setTitle(post.title);

                return () => {
                    setDiffs([]);
                    editor.clearHistory();
                };
            } else {
                editor.setValue('');
                setTitle('');

                return () => {
                    setDiffs([]);
                };
            }
        }
    }, [post, editor]);

    const handleSaveClick = () => {
        setEditable(false);
        fetch(`/api/posts`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: title,
                body: editor.getValue()
            })
        })
            .then((response) => {
                if (response.ok) {
                    return response.json().then((response) => {
                        setPost(response);
                        history.replace(`/p/${response.id}`);
                    });
                } else {
                    response.json().then((response) => displayError(response));
                }
            })
            .finally(() => {
                setEditable(true);
            });
    };

    const handleUpdateClick = () => {
        setEditable(false);
        fetch(`/api/posts`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: title,
                body: editor.getValue(),
                parent_id: post.id
            })
        })
            .then((response) => {
                if (response.ok) {
                    return response.json().then((response) => {
                        setPost(response);
                    });
                } else {
                    response.json().then((response) => displayError(response));
                }
            })
            .finally(() => {
                setEditable(true);
            });
    };

    const handleOverwriteClick = () => {
        setEditable(false);
        fetch(`/api/posts/${post.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: title,
                body: editor.getValue()
            })
        })
            .then((response) => {
                if (response.ok) {
                    return response.json().then((response) => {
                        setPost(response);
                    });
                } else {
                    response.json().then((response) => displayError(response));
                }
            })
            .finally(() => {
                setEditable(true);
            });
    };

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const closeModal = () => setIsOpen(false);
    const handleOpenHistory = () => {
        setIsOpen(true);
        fetch(`/api/posts/history/${post.id}`)
            .then((response) => response.json())
            .then((posts) => {
                if (posts.length > 0) {
                    for (let i = 0; i < posts.length; i++) {
                        posts[i].created_at = new Date(posts[i].created_at).toLocaleString();
                        posts[i].updated_at = new Date(posts[i].updated_at).toLocaleString();
                    }
                    const text = posts[0].body;
                    delete posts[0]['body'];
                    posts[0]['body'] = text;

                    const diffHtmlList = [];
                    for (let i = 0; i + 1 < posts.length; i++) {
                        const patch = Diff.createPatch(posts[i].title, posts[i + 1].body, posts[i].body, posts[i + 1].title, posts[i].title);
                        diffHtmlList.push({
                            doc: Diff2html.html(patch, {
                                    outputFormat: OutputFormatType.SIDE_BY_SIDE
                                }),
                            post: posts[i]
                        });
                    }
                    setDiffs(diffHtmlList);
                }
            });
    };

    return (
        <div id="content">
            <div id="header">
                <nav id="navbar">
                    <div>
                        <button onClick={() => history.goBack()}>&lt; BACK</button>
                    </div>
                    <div>
                        { !post && <button disabled={!editable} onClick={handleSaveClick}>SAVE</button> }
                        { post && <>
                            <button disabled={!editable} onClick={handleUpdateClick}>UPDATE</button>
                            <button disabled={!editable} onClick={handleOverwriteClick}>OVERWRITE</button>
                            <button onClick={handleOpenHistory}>HISTORY</button>
                        </> }
                    </div>
                </nav>
            </div>
            <div id="main">
                <div id="toc" ref={tocElement}>
                </div>
                <div id="editor" ref={editorElement}>
                    <input placeholder="TITLE" value={title} onChange={(e) => setTitle(e.target.value)} id="title" style={{width: '100%', fontSize: 'xx-large'}} />
                    {<textarea style={{display: 'none'}} ref={textAreaElement} />}
                </div>
            </div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                contentLabel="open history"
            >
                {diffs && diffs.map((diff, i) => {
                return (
                    <div key={`diff${i}`}>
                        <p>updated at {diff.post.updated_at}</p>
                        <span dangerouslySetInnerHTML={{ __html: diff.doc }} />
                    </div>
                );
            })}
            </Modal>
        </div>
    );
};

export default PostComponent;
