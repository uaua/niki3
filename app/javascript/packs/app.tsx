// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';
import PostsComponent from 'components/Posts';
import PostComponent from 'components/Post';
import { ToastContainer } from "react-toastify";
import Modal from 'react-modal';
import 'react-toastify/dist/ReactToastify.min.css';

Modal.setAppElement('#app');

document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
        <>
            <HashRouter>
                <Switch>
                    <Route path="/p/:id?" component={PostComponent} />
                    <Route exact path="/" component={PostsComponent} />
                </Switch>
            </HashRouter>
            <ToastContainer autoClose={3000} pauseOnHover={false} pauseOnFocusLoss={false} />
        </>,
        document.body.appendChild(document.getElementById('app')),
    )
});
