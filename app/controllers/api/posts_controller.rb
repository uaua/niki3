class Api::PostsController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActiveRecord::RecordNotUnique, with: :record_not_unique

  # GET /posts
  def index
    @posts = Post.active_list
  end

  def search
    q = "%#{params[:text].gsub(/[\\%_]/){|m| "\\#{m}"}}%"
    @posts = Post.where('body LIKE ?', q)
                 .or(Post.where('title LIKE ?', q))
                 .where(id: Post.select("MAX(id) as id")
                                .group(:root_id))
                 .where(deleted: false)
                 .order(updated_at: "DESC")
    render :index
  end

  # DELETE /posts/1
  def destroy
    @post = Post.find(params[:id])
    raise ActiveRecord::RecordNotFound unless @post
    @post.deleted = true

    if @post.save
      render :show, status: :ok
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  # POST /posts
  def create
    @post = Post.new(post_params)

    if @post.save
      render :show, status: :created
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def show
    @post = Post.find_by(id: Post.select("MAX(id) as id").where(root_id: params[:id]))
    raise ActiveRecord::RecordNotFound unless @post
  end

  # PATCH/PUT /posts/1
  def update
    @post = Post.find(params[:id])
    res = @post.update(update_params)

    if res
      render :show, status: :ok
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def history
    post = Post.find(params[:id])
    raise ActiveRecord::RecordNotFound unless post
    @posts = Post.where(root_id: post.root_id).where("id <= ?", post.id).order(updated_at: "DESC")
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.permit(
        :title, :body, :parent_id
    )
  end

  def update_params
    params.permit(
        :title, :body
    )
  end

  def record_not_found(exception)
    render json: { error: "not found" }, status: :not_found
  end

  def record_not_unique(exception)
    render json: { error: 'parent is already outdated' }, status: 422
  end
end
