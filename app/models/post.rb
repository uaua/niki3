class Post < ApplicationRecord
  belongs_to :parent, class_name: "Post", :foreign_key => "parent_id", optional: true
  belongs_to :root, class_name: "Post", :foreign_key => "root_id", optional: true

  validates :title, length: { minimum: 1, allow_nil: false, message: "TITLE require" }
  validates :body, length: { minimum: 0, allow_nil: false, message: "BODY require" }
  validate :parent_exists
  validate :outdated_post, on: :create
  validate :archived_post, on: :update
  validate :post_changed, on: :create
  validate :parent_post_already_deleted, on: :create
  validate :this_post_already_deleted, on: :update

  before_validation :prepare_validation
  before_save :prepare_save
  #before_update :prepare_update
  after_update :post_update
  after_save :post_save

  scope :active_list, -> { Post.unscoped.where(id: Post.unscoped.select("MAX(id) as id").group(:root_id)).where(deleted: false).order(updated_at: "DESC") }

  def prepare_validation
  end

  def parent_post_already_deleted
    return if parent_id.nil?

    if Post.find_by(id: parent_id)&.deleted
      errors.add(:parent_id, :parent_post_already_deleted)
    end
  end

  def this_post_already_deleted
    if Post.find(id).deleted
      errors.add(:deleted, :this_post_already_deleted)
    end
  end

  def parent_exists
    return if parent_id.nil?

    unless Post.exists?(id: parent_id)
      errors.add(:parent_id, :parent_does_not_exist)
    end
  end

  def outdated_post
    return if parent_id.nil?

    if Post.find_by(id: parent_id)&.archived
      errors.add(:parent_id, :parent_post_is_outdated)
    end
  end

  def archived_post
    if (title_changed? || body_changed?) && archived
      errors.add(:archived, :this_post_was_archived)
    end
  end

  def post_changed
    parent = Post.find_by(id: parent_id)
    if parent&.title == title && parent&.body == body
      errors.add(:base, :no_changes)
    end
  end

  def prepare_save
    unless parent_id.nil?
      parent.lock!
      self.root_id = parent.root_id
    end

    self
  end

  def prepare_update
    if Post.exists?(parent_id: id)
      throw :abort
    end

    self
  end

  def post_save
    if root_id.nil?
      self.root_id = id
      self.save!
    end
    unless parent_id.nil?
      parent.record_timestamps = false
      parent.archived = true
      parent.save!
    end
  end

  def post_update
    #if Post.find_by(id: parent_id)&.archived
    if Post.find(id).archived
      errors.add(:parent_id, :this_post_was_archived)
      raise ActiveRecord::Rollback
    end
  end
end
