require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    Post.delete_all
  end

  teardown do
    # コントローラがキャッシュを使っている場合、テスト後にリセットしておくとよい
    Rails.cache.clear
  end

  test "add post" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_\n*b*",
      }
      assert_response :success

      res = JSON.parse(body)
      assert_equal "title", res['title']
      assert_equal "_a_\n*b*", res['body']
      assert_nil res['parent_id']
      assert_equal res['id'], res['root_id']
    end
  end

  test "add post with root_id" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_\n*b*",
                                     root_id: 114514
      }
      assert_response :success

      res = JSON.parse(body)
      assert_equal "title", res['title']
      assert_equal "_a_\n*b*", res['body']
      assert_nil res['parent_id']
      assert_equal res['id'], res['root_id']
    end
  end

  test "add child post" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title1",
                                     body: "_a_ *b*"
      }
      assert_response :success

      res1 = JSON.parse(body)
      assert_equal "title1", res1['title']
      assert_equal "_a_ *b*", res1['body']
      assert_nil res1['parent_id']

      post api_posts_path, params: { title: "title2",
                                     body: "karada",
                                     parent_id: res1['id']
      }
      assert_response :success

      res2 = JSON.parse(body)
      assert_equal "title2", res2['title']
      assert_equal "karada", res2['body']
      assert_equal res1['id'], res2['parent_id']
      assert_equal res1['id'], res2['root_id']
    end
  end

  test "edit child post" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title1",
                                     body: "_a_ *b*"
      }
      assert_response :success

      res1 = JSON.parse(body)
      assert_equal "title1", res1['title']
      assert_equal "_a_ *b*", res1['body']
      assert_nil res1['parent_id']

      post api_posts_path, params: { title: "title",
                                     body: "karada",
                                     parent_id: res1['id']
      }
      assert_response :success

      res2 = JSON.parse(body)
      assert_equal "title", res2['title']
      assert_equal "karada", res2['body']
      assert_equal res1['id'], res2['parent_id']
      assert_equal res1['id'], res2['root_id']

      patch api_post_path(res2['id']), params: { body: "changed body2",
      }
      assert_response :success

      edited = JSON.parse(body)
      assert_equal "title", edited['title']
      assert_equal "changed body2", edited['body']
      assert_equal res1['id'], edited['parent_id']
      assert_equal res1['id'], edited['root_id']
    end
  end

  test "failed adding post" do
    assert_difference 'Post.count', 0 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
                                     parent_id: 114514
      }
      assert_response 422
    end
  end

  test "failed adding outdated post" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      res = JSON.parse(body)

      post api_posts_path, params: { title: "title1",
                                     body: "body1",
                                     parent_id: res['id']
      }
      assert_response :success

      post api_posts_path, params: { title: "title2",
                                     body: "body2",
                                     parent_id: res['id']
      }
      assert_response 422
    end
  end

  test "failed same time post" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      res = JSON.parse(body)

      threads = []
      post1_body = nil
      post2_body = nil
      l = false
      threads << Thread.new do
        ActiveRecord::Base.connection_pool.with_connection do
          l = true
          post api_posts_path, params: { title: "title1",
                                         body: "body1",
                                         parent_id: res['id']
          }
          post1_body = body
          assert_response :success
        end
      end

      threads << Thread.new do
        sleep(0.001) until l
        ActiveRecord::Base.connection_pool.with_connection do
          post api_posts_path, params: { title: "title2",
                                         body: "body2",
                                         parent_id: res['id']
          }
          assert_response 422
        end
        post2_body = body
      end
      threads.each(&:join)
    end
  end

  test "failed editing outdated post" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      res = JSON.parse(body)

      post api_posts_path, params: { title: "title1",
                                     body: "body1",
                                     parent_id: res['id']
      }
      assert_response :success

      patch api_post_path(res['id']), params: { title: "edited title",
                                                body: "edited body"
      }
      assert_response 422
      # p body
      # p Post.all
    end
  end

  test "failed same time edit" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      res = JSON.parse(body)
      # p res

      threads = []
      post1_body = nil
      post2_body = nil
      l = false
      threads << Thread.new do
        ActiveRecord::Base.connection_pool.with_connection do
          l = true
          post api_posts_path, params: { title: "title1",
                                         body: "body1",
                                         parent_id: res['id']
          }
          assert_response :success
          post1_body = body
        end
      end

      threads << Thread.new do
        sleep(0.001) until l
        ActiveRecord::Base.connection_pool.with_connection do
          patch api_post_path(res['id']), params: { title: "edited title",
                                                    body: "edited body"
          }
          #assert_response 422
        end
        post2_body = body
      end
      threads.each(&:join)
      # p post1_body, post2_body
    end
  end

  test "edit post" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*"
      }
      assert_response :success

      post = JSON.parse(body)

      patch api_post_path(post['id']), params: { title: "changed title",
                                                 body: "changed body",
                                                 parent_id: 114514
      }
      assert_response :success

      edited = JSON.parse(body)
      assert_equal "changed title", edited['title']
      assert_equal "changed body", edited['body']
      assert_nil edited['parent_id']
      assert_equal edited['id'], edited['root_id']

      patch api_post_path(post['id']), params: { title: "changed title2",
      }
      assert_response :success

      edited = JSON.parse(body)
      assert_equal "changed title2", edited['title']
      assert_equal "changed body", edited['body']
      assert_nil edited['parent_id']
      assert_equal edited['id'], edited['root_id']

      patch api_post_path(post['id']), params: { body: "changed body2",
      }
      assert_response :success

      edited = JSON.parse(body)
      assert_equal "changed title2", edited['title']
      assert_equal "changed body2", edited['body']
      assert_nil edited['parent_id']
      assert_equal edited['id'], edited['root_id']
    end
  end

  test "failed edit" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "body"
      }
      assert_response :success

      post = JSON.parse(body)

      patch api_post_path(post['id'] + 1), params: {
          title: "changed title",
          body: "changed body",
      }
      assert_response :missing
    end
  end

  test "failed add child because of no changes" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "body"
      }
      assert_response :success

      post = JSON.parse(body)

      post api_posts_path, params: { title: "title",
                                     body: "body",
                                     parent_id: post['id']
      }
      assert_response 422
    end
  end

  test "show all posts with deleted" do
    assert_difference 'Post.count', 3 do
      post api_posts_path, params: { title: "title1",
                                     body: "body1"
      }
      assert_response :success

      post1 = JSON.parse(body)
      post api_posts_path, params: { title: "title2",
                                     body: "body2"
      }
      assert_response :success

      post2 = JSON.parse(body)
      post api_posts_path, params: { title: "title3",
                                     body: "body3"
      }
      assert_response :success

      post3 = JSON.parse(body)

      delete api_post_path(post2['id'])

      get api_posts_path
      posts = JSON.parse(body)
      assert_equal 2, posts.length
      assert_equal posts[0]['id'], post1['id']
      assert_equal posts[1]['id'], post3['id']
    end
  end

  test "delete post" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "body"
      }
      assert_response :success

      post = JSON.parse(body)
      assert_not post['deleted']

      delete api_post_path(post['id'])
      post = JSON.parse(body)
      assert post['deleted']
    end
  end

  test "failed deleting outdated post" do
    assert_difference 'Post.count', 2 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      res = JSON.parse(body)

      post api_posts_path, params: { title: "title1",
                                     body: "body1",
                                     parent_id: res['id']
      }
      assert_response :success

      delete api_post_path(res['id'])
      assert_response 422
    end
  end

  test "failed adding childpost because of deleted" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      post = JSON.parse(body)

      delete api_post_path(post['id'])
      post = JSON.parse(body)
      assert post['deleted']

      post api_posts_path, params: { title: "title1",
                                     body: "body1",
                                     parent_id: post['id']
      }
      assert_response 422
    end
  end

  test "failed editing because of deleted" do
    assert_difference 'Post.count', 1 do
      post api_posts_path, params: { title: "title",
                                     body: "_a_ *b*",
      }
      assert_response :success
      post = JSON.parse(body)

      delete api_post_path(post['id'])
      post = JSON.parse(body)
      assert post['deleted']

      patch api_post_path(post['id']), params: {
          title: "title1",
          body: "body1",
      }
      assert_response 422
    end
  end
end
