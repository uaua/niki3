require 'test_helper'

class PostTest < ActiveSupport::TestCase
  setup do
    Post.delete_all
  end

  test "should be valid" do
    post = Post.new(title: "   ", body: "*iei*\n_abc_")
    assert post.valid?
  end

  test "should be invalid" do
    post = Post.new(title: "", body: "*iei*\n_abc_")
    assert_not post.valid?
  end

  test "parent does not exist" do
    post = Post.new(title: "   ", body: "*iei*\n_abc_", parent_id: 0)
    assert_not post.valid?
  end

  test "parent exists" do
    par = Post.new(title: "title", body: "body")
    par.save!
    assert_equal(par.id, par.root_id)
    assert_nil(par.parent_id)
    assert_not par.archived

    post1 = Post.new(title: "   ", body: "*iei*\n_abc_", parent_id: par.id)
    assert post1.valid?
    post1.save!
    par.reload
    assert_equal(par.id, post1.root_id)
    assert par.archived
    assert_not post1.archived

    post2 = Post.new(title: "2", body: "*22", parent_id: post1.id)
    assert post2.valid?
    post2.save!
    post1.reload
    assert_equal(par.id, post2.root_id)
    assert post1.archived
    assert_not post2.archived
  end

  test "parent default value should be nil" do
    post = Post.new(title: "oya", body: "*iei*\n_abc_")
    assert_nil(post.parent_id)
  end

  test "should be invalid title" do
    post = Post.new(body: "*zxc*\n_asd_")
    assert_not post.valid?
  end

  test "should be invalid body" do
    post = Post.new(title: "titttle")
    assert_not post.valid?
  end

  test "should be outdated" do
    root = Post.new(title: "root title", body: "root body")
    assert root.valid?
    root.save

    post1 = Post.new(title: "post1 title", body: "post1 body", parent_id: root.id)
    assert post1.valid?
    post2 = Post.new(title: "post2 title", body: "post2 body", parent_id: root.id)
    assert post2.valid?

    post1.save
    assert_equal root.id, post1.root_id
    assert_not post2.valid?
  end

  test "active list" do
    roots = []
    children = []
    3.times do |i|
      root = Post.new(title: "root#{i} title", body: "root#{i} body")
      assert root.valid?
      root.save
      roots.push(root)
      children.push(root)
    end

    4.times do |j|
      roots.each_with_index do |root, i|
        post = Post.new(title: "root#{i} child#{j} title", body: "root#{i} child#{i} body", parent_id: children[i].id)
        assert post.valid?
        post.save
        children[i] = post
      end
    end

    active_posts = Post.active_list
    assert_equal children[0].id, active_posts[0].id
    assert_equal children[1].id, active_posts[1].id
    assert_equal children[2].id, active_posts[2].id
  end
end
