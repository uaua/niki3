class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.integer :parent, null: true
      t.integer :root, null: true

      t.timestamps
    end
  end
end