class AddColumnToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :frozen, :boolean, default: false, null: false
  end
end