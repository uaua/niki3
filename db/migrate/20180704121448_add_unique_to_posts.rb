class AddUniqueToPosts < ActiveRecord::Migration[5.2]
  def change
    add_index  :posts, :parent_id, unique: true
  end
end