class RenameColumnToPosts < ActiveRecord::Migration[5.2]
  def change
    rename_column :posts, :parent, :parent_id
    rename_column :posts, :root, :root_id
  end
end