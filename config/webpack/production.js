process.env.NODE_ENV = process.env.NODE_ENV || 'production'

const environment = require('./environment')

const sassLoader = environment.loaders.get('sass');
const cssLoader = sassLoader.use.find(loader => loader.loader === 'css-loader');

cssLoader.options = Object.assign(cssLoader.options, {
    modules: false,
    localIdentName: '[path][name]__[local]--[hash:base64:5]'
});

module.exports = environment.toWebpackConfig()
