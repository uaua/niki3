Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'page#home'
  get '/p', to: 'page#home'
  get '/p/:id', to: 'page#home'
  get '/s/:id', to: 'page#home'

  namespace :api, format: 'json' do
    resources :posts, only: [:index, :show, :create, :update, :destroy]
    get '/posts/history/:id', to: 'posts#history'
    post '/posts/search', to: 'posts#search'
  end
end
